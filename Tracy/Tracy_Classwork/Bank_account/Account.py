__author__ = 'Tracy'

# Most of this is stolen from Hal's lecture.

# It is good general practice to name the file the same name as the class that it contains.

# This is setting up the generic "account" class.


class Account:
    # This sets up the generic instantiator, for lack of a better term... The terms below it assigns the value passed
    # in to the self.whatever. NEED AN INIT FOR EACH CLASS, BECAUSE THEY'RE SEPARATE CLASSES. Sub-classes only inherit
    # methods from the parent.
    def __init__(self, in_holder, in_number, in_balance):
        self.holder = in_holder
        self.number = in_number
        self.balance = in_balance

    # This sets up the definition of the "deposit" method.
    def deposit(self, amount):
        self.balance = self.balance + amount  # Can also use self.balance = self.balance += amount

    # This sets up the "transfer" method.
    def transfer(self, amount):
        if self.balance - amount < 0:
            return "Your balance is not sufficient to allow a transfer out."
        else:
            self.balance -= amount  # This iterates a subtraction.
            return "Transfer completed."

    # This sets up the "withdraw" method.
    def withdraw(self, amount):
        if self.balance - amount < 0:
            return "Your balance is not sufficient to allow a withdrawal."
        else:
            self.balance = self.balance - amount
            return "Withdrawal completed."

    # This sets up the "balance" method.
    def balance(self):
        return self.balance


# This sets up the generic checking account.
class CheckingAccount(Account):
    # This sets up the generics instantiator, for lack of a better term... The terms below it assigns the value passed
    # in to the self.whatever.
    def __init__(self, in_holder, in_number, in_balance):
        self.holder = in_holder
        self.number = in_number
        self.balance = in_balance


# This sets up the generic savings account.
class SavingsAccount(Account):
    # This sets up the generics instantiator, for lack of a better term... The terms below it assigns the value passed
    # in to the self.whatever.
    def __init__(self, in_holder, in_number, in_balance):
        self.holder = in_holder
        self.number = in_number
        self.balance = in_balance


# This sets up the generic credit account.
class CreditAccount(Account):
    # This sets up the generics instantiator, for lack of a better term... The terms below it assigns the value passed
    # in to the self.whatever.
    def __init__(self, in_holder, in_number, in_balance, in_credit_line=1500):
        self.holder = in_holder
        self.number = in_number
        self.balance = in_balance
        self.credit_line = in_credit_line

    # This sets up the payment method.
    def payment(self, amount):
        self.balance -= amount