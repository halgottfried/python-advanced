

__author__ = 'Tracy'

# # This, also, is stolen from Hal's lecture...
#
# # This creates the instance of the account. We need to feed it an account holder name, an account number, and a balance.
# # Credit line is taken care of in the method.
# # The first "account" is the class name (data type), and the second "account" is the object instance. This creates the
# # first instance of an account-class object.
# CheckingAccount = Account.Account("Tracy", 123456, 100)
#
# # This creates an instance of a savings account. We need to feed it an account holder name, an account number, and a
# # balance. Credit line is taken care of in the method.
# # The first "account" is the class name (data type), and the second "account" is the object instance. This creates the
# # first instance of an account-class object.
# SavingsAccount = Account.Account("Tracy", 456789, 100)
#
# # This creates an instance of a credit account. We need to feed it an account holder name, an account number, and a
# # balance. Credit line is taken care of in the method.
# # The first "account" is the class name (data type), and the second "account" is the object instance. This creates the
# # first instance of an account-class object.
# CreditAccount = Account.Account("Tracy", 789123, 100)

# Ask the user for a name, type of account, and initial deposit or credit limit.

from Account import Account
from Account import CheckingAccount
from Account import SavingsAccount
from Account import CreditAccount

Name = input("What is the account holder's name? ")

print("The account holder's name is:  " + Name + ".")

AcctNo = input("What account number would you like to assign to this account? ")

print("The account number you have assigned is:  " + str(AcctNo) + ".")

AcctType = input("What type of account would the account holder like to open? A) Checking, B) Savings, or C) Credit? ")

# if AcctType = "a":
#     AcctType = "A"
# elif AcctType = "b":
#     AcctType = "B"
# elif AcctType = "c":
#     AcctType = "C"
# else:
#     print ("That is not a valid selection.")

if AcctType == "A" or AcctType == "a":
    AcctOpened = "You've chosen to open a checking account. "
    OpenBal = input("You've chosen to open a checking account. What is the opening balance on this account? ")
elif AcctType == "B" or AcctType == "b":
    AcctOpened = "You've chosen to open a savings account. "
    OpenBal = input("You've chosen to open a savings account. What is the opening balance on this account? ")
elif AcctType == "C" or AcctType == "c":
    AcctOpened = "You've chosen to open a credit account. "
    OpenBal = input("""You've chosen to open a credit account with a credit limit of $1500.00.
    What is the opening balance on this account? """)
else:
    print("That is an invalid selection. The program will now exit.")
    exit()

print(AcctOpened + "The account holder's name is " + Name + ". The opening balance is " + str(OpenBal) + ".")

error_check = input("Is this correct? Enter Y for yes, or N for no.")

if error_check == "Y" or error_check == "y":
    if AcctType == "A" or AcctType == "a":
        AcctName = str(Name) + "Chk"
        print(AcctName)
        AcctName = CheckingAccount(Name, AcctNo, OpenBal)
        print(Account.balance(AcctName))
    elif AcctType == "B" or AcctType == "b":
        AcctName = str(Name) + "Sav"
        print(AcctName)
        AcctName = SavingsAccount(Name, AcctNo, OpenBal)
    elif AcctType == "C" or AcctType == "c":
        AcctName = str(Name) + "Credit"
        print(AcctName)
        AcctName = CreditAccount(Name, AcctNo, OpenBal)
    else:
        print("That is an invalid selection. The program will now exit.")
        exit()

else:
    print("Apparently, one of us has made a mistake. The program will now exit.")
    exit()
