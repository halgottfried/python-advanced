class ParkingLotBuilder:

     # This first section is defines the function that allows a new instance of the "object" to be created and identify
     # itself.

     def __init__(self):
      print("A new parking lot blueprint has been created.")
      self.numSpaces = 0
      self.carsInLot = 0

# I include self as an input to allow me to have access to the global variables.

     def LotSize(self,numSpaces):
      print("Your parking lot has spaces assigned to it now")
      self.numSpaces = numSpaces

     def ParkingCheck(self,IncommingCars):
      print("You are trying to park " + repr(IncommingCars) + " in your new lot")
      if (IncommingCars <= self.numSpaces):
           print ("There is enough room for those cars.")
           self.numSpaces = (self.numSpaces - IncommingCars)
           self.carsInLot = IncommingCars
           print ("You have " + repr(self.numSpaces) + " left in your lot.")
      else:
           print("You are not able to park all of those cars here.")
           # Later we can create another lot here and move those cars there.

     def CarsLeavingCheck(self,ExitingCars):
      print("You have " + repr(ExitingCars) + " leaving your lot")
      self.carsInLot = (self.carsInLot - ExitingCars)
      self.numSpaces =  (self.numSpaces + ExitingCars)
      print("There are now " + repr(self.carsInLot) + " and you have " + repr(self.numSpaces) + " open spaces left")
