import ParkingLotBuilder

# -------------------------------------------------------------------------

# Follow Up to Class Review:
# The not callable error message you see on JoeKamSouthLot = ParkingLotBuilder is due to that when you issue "import ParkingLot Builder"
# you're actually importing the "ParkingLotBuilder" _module_, not the "ParkingLotBuilder" class inside the module.

# To address there are two possible ways:
#  import ParkingLotBuilder
#  a = ParkingLotBuilder.ParkingLotBuilder(25) # access the class inside module (or the class in it SELF)

# The otehr method I gave some of you a sneak peak in class
# From ParkingLotBuilder import ParkingLotBuilder   # insert in the current namespace (the instance) the ParkingLotBuilder class
# a = ParkingLotBuilder(25)

# -----------------------------------------------------------------------------

# Here we create an instance of our ParkingLotBuilder to create a new lot with the name JoeKamSouthLot with 25 spaces.
# You can confirm this as the print statment should be returned to you from the module.

JoeKamSouthLot = ParkingLotBuilder.ParkingLotBuilder()

# Specify that our new parking lot "blueprint" has 25 parking 25 spots in it.

JoeKamSouthLot.LotSize(25)

JoeKamSouthLot.ParkingCheck(5)  # This will work and allow me to park however (50) will return a failure because it is to many

JoeKamSouthLot.ParkingCheck(10) # Parking 10 more cars

JoeKamSouthLot.CarsLeavingCheck(3) # Three Cars Leave
