__author__ = 'Tracy'

# This is parkinglot.py. It defines the parking lot class and defines the methods used.
# It keeps track of how many spaces there are in the parking lot, altogether, how many are
# full, and how many are empty.

class parkinglot:
    def __init__(self, in_LotSize):
        # create number of spaces overall
        self.SpacesLeft = in_LotSize
        # creates global variable NumToPark. This means we will be getting NumToPark, but not necessarily in this function.
        # self.NumToPark = 0

    # def SpacesLeft(self):
    #     self.SpacesLeft = self.SpacesLeft
    #     return self.SpacesLeft

    def CheckAvailability(self, NumToPark):
        if NumToPark > self.SpacesLeft:
            print("There isn't enough room.")
        else:
            self.SpacesLeft -= NumToPark
            print("I've parked your cars!")
            return self.SpacesLeft



