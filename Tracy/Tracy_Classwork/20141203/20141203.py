__author__ = 'Tracy'


mystuff = {'apples': "I am apples."} #Dictionary, where "apples" is the key and "I am apples" is the value.

print_mystuff['apples']                 #Tells Python to get the "apples" key from the "mystuff" dictionary.

#####################################

def apple():
    print("I am apples.")                #This is a function called "mystuff.py" that does what the above dictionary stuff does.

    // mystuff.py is the name of the function

import mystuff                          #This imports the function we've defined and lets us do what we did in the first two lines
mystuff.apple()                         #Has to be in path.

//////////////////////////////////////////////
