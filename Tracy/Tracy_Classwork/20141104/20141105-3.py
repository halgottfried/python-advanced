__author__ = 'Tracy'

import sys  #system package

ArrayStuff = ("This", "is", "my", "array")

try:
    print(ArrayStuff[5])

except:
    e=sys.exc_info()[0]     #exc is like eval. Only shows first error. Can get length of the array and while loop it to show all
    print (e)
   # ZeroDivisionError

