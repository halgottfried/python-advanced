class BalanceError(Exception):
      value = "Sorry you only have $%6.2f in your account"

class BankAccount:
    def __init__(self, initialAmount):
       self.balance = initialAmount
       print "Account created with balance %5.2f" % self.balance

    def deposit(self, amount):
       self.balance = self.balance + amount

    def withdraw(self, amount):
       if self.balance >= amount:
          self.balance = self.balance - amount
       else:
          raise BalanceError, BalanceError.value % self.balance

    def checkBalance(self):
       return self.balance

    def transfer(self, amount, account):
       try:
          self.withdraw(amount)
          account.deposit(amount)
       except BalanceError:
          print BalanceError.value % self.balance
