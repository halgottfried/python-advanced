Opening and closing files

Open file object
Text in = open ("file.txt", r)		r lets you read from the file
		Must be in path that you're in, or have to provide path
		r = read
		w = write
		a = append
		r+ = read and write
		a+ = append and read
		
textfile = open ("file.txt", r)

Reading text files

	textfile.read(1)		reads first character
	print(textfile.read(1)) prints read text
	textfile.read(5)        starts after last read
	textfile.read()         reads all characters in the file, including all lines
    textfile.readline()     reads all in current line. Need to create pointer in file to change lines.

A list is an array of strings.

Writing





Task:

Import from sys, argv class                     #See below
Create code that:
    Takes command line arguments (python file.py joe.txt)
    Opens a file from command argument for read()
    Takes user input for text
    Takes user input and appends to file


FROM SYS IMPORT ARGV
script, filename = argv
txt = open(filename)

BITBUCKET

Master is the project

    Can clone
        Have own copy when cloned

    Can fork
        Share copy, but your version.
        Make changes
            Pull
            Commit changes
