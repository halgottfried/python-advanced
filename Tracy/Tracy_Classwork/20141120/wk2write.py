__author__ = 'Tracy'

inputtext = "Greetings, fellow Martians!"   #put text into variable.
#
# outputfile = open("write_text.txt","w")     #open a new file called write_text.txt for writing
#
# outputfile.write(inputtext)                 #write the variable contents into the text file

outputfile = open("write_text.txt","a")         #open a file to append info

outputfile.write("\n")                          #Need to do this to put input on a new line
outputfile.write(inputtext)