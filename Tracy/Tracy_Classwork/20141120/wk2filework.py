__author__ = 'Tracy'

# textfile = open("text.txt","r")
#
# print(textfile.read(1))
# print(textfile.read(5))         #starts after the previous read
#
# textfile.close()
#
# textfile = open("text.txt", "r")
#
# AllOfIt = textfile.read()
#
# print (AllOfIt)

textfile = open("text.txt","r")

#print(textfile.readline())

lines = textfile.readline()
print(lines)
#print(len(lines))                   #gives length of first line, including newline character

for lines in lines:                #as long as there is something in lines, keep going
    print (lines)
textfile.close()

