from sys import argv

__author__ = 'Tracy'

# Task:
#
# Import from sys, argv class                     #See below
# Create code that:
#     Takes command line arguments (python file.py joe.txt)
#     Opens a file from command argument for read()
#     Takes user input for text
#     Takes user input and appends to file
#
#
# FROM SYS IMPORT ARGV
# script, filename = argv
# txt = open(filename)

from sys import argv                            #imports argv from sys so you can use command line inputs
script, filename = argv                         #??? storing from command line variables
txt = open(filename)                            #opens text file with file name

inputtext = input("Type some stuff... ")
txt = open(filename,"a")                        #open a file to append info

txt.write("\n")                          #Need to do this to put input on a new line
txt.write(inputtext)

txt.close()